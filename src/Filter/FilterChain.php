<?php

namespace G3Counter\Filter;

use G3Counter\Models\Word;

class FilterChain implements Filter
{

    private $filters = [];

    public function __construct($filterOrFilters)
    {
        if (is_array($filterOrFilters)) {
            $this->filters = $filterOrFilters;
        } else {
            $this->filters[] = $filterOrFilters;
        }
    }

    public function chain(Filter $filter) {
        $filters = $this->filters;
        $filters[] = $filter;
        return new FilterChain($filters);
    }

    public function __invoke(Word $word)
    {
        foreach ($this->filters as $filter) {
            $passesFilter = $filter->__invoke($word);
            if ($passesFilter === false) return $passesFilter;
        }

        return true;
    }
}