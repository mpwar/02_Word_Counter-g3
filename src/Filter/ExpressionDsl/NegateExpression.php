<?php

namespace G3Counter\Filter\ExpressionDsl;

use G3Counter\Models\Word;

class NegateExpression extends Expression
{

    public function __invoke(Word $word)
    {
        return !$this->filter->__invoke($word);
    }

}