<?php

namespace G3Counter\Filter\ConcreteFilter;

use G3Counter\Filter\Filter;
use G3Counter\Models\Word;

class KeywordFilter implements Filter
{

    private $keywordsToTest = [];

    public function __construct(array $keywords) {
        $this->keywordsToTest = $keywords;
    }

    public function __invoke(Word $word)
    {
        return in_array(strtolower($word->get()), $this->keywordsToTest);
    }

}