<?php

namespace G3Counter\Counter\ConcreteCounter;

use G3Counter\Counter\Counter;
use G3Counter\Filter\Filter;
use G3Counter\Models\Text;

class WordCounter implements Counter
{
    public function __invoke(Text $text, Filter $filter = null)
    {

        $words = $text->getWords();

        if(is_null($filter))
        {
            return count($words);
        }

        $counter = 0;

        foreach($words as $word) {
            if ($filter($word)) {
                $counter++;
            }
        }

        return $counter;

    }
}