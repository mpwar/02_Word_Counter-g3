<?php

namespace G3Counter\Models;


class Word
{
    private $word;

    public function __construct($word)
    {
        $this->word = $word;
    }

    public function get() {
        return $this->word;
    }

}